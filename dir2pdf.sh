#!/bin/bash

read_p(){
	# read -p is not compatible with POSIX shell
	echo -n "$@"
	read -r answer
	echo "$answer" >/dev/null
}

if [ ! -x "$(which rsvg-convert)" ]; then
	echo "rsvg-convert not found, cannot continue!"
	rsvg-convert
	exit 1
fi

if [ ! -z "$1" ]
	then
		target_dir="$1"
	else
		read_p "Введите путь к директории с сохраненными книгами:"
		target_dir="$answer"
fi

echo "target_dir: $target_dir"
echo " "

cd "$target_dir"

while read line
do
	# надо бы распаралелливаться
	echo "Processing $line"
	if [ -f "${line}.pdf" ]; then
		echo "${line}.pdf уже существует, пропускаем..."
		echo " "
		continue
	fi
	# надо бы без cd
	cd "$line"
	# защита от запуска в пустой директории
	if ls *.svg >/dev/null; then 
		rsvg-convert -f pdf -o "../${PWD##*/}.pdf" $(ls -1v *.svg)
	fi
	cd ..
done < <(find . -mindepth 1 -type d)
