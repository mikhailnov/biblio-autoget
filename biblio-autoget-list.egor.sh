#!/bin/bash
user="$(whoami)"
# запомним директорию, откуда начинаем работать
dir0="$(pwd)"
# список идентификаторов книг для скачивания
list="books.list"
# папка, в которую сохранять книги
# $(pwd)/save_dir значит папка save_dir внутри папки скрипта
save_dir="$(pwd)/save_dir"
log_dir="$(pwd)/log_dir"
mkdir -p "$log_dir" && log_file="${log_dir}/log_$(date).log"

# --window-size только для размера окна при скриншоте, по факту это быссмысленно для нас, т.е. не работает 
alias firefox="firefox --window-size 1200,700"
pkill firefox
pkill -9 firefox

# теперь подставим в настройки Firefox папку для сохранения книг
# если профиля Firefox нет (был удален), то запустим Firefox, чтобы он создал этот профиль, а затем выключим его и исправим профиль
if [ ! -f "$HOME/.mozilla/firefox/profiles.ini" ]; then
	firefox -headless &
	sleep 3
	while true
	do
		( ps aux | grep "$user" | grep -Fq "$(which firefox)" ) && pkill firefox
		# проверим, убился ли фаерфокс
		if ps aux | grep "$user" | grep -Fq "$(which firefox)"
			then
				pkill -9 firefox; pkill -9 firefox; pkill -9 firefox
				break
			else
				break
		fi
	done
fi
firefox_prefsjs="$HOME/.mozilla/firefox/$(cat "$HOME/.mozilla/firefox/profiles.ini" | grep 'Path=' | awk -F '=' '{print $2}')/prefs.js"
if [ ! -f "$HOME/.mozilla/firefox/profiles.ini" ]; then
	echo "Не найден профиль Firefox, и его не получилось создать, не можем продолжить работать!"
	exit 1
fi
cp -v "$firefox_prefsjs" "${firefox_prefsjs}.bak"
sed "/browser.download.dir/d" -i "$firefox_prefsjs"
echo user_pref\(\"browser.download.dir\"\, \"${save_dir}/0\"\)\; | tee -a "$firefox_prefsjs"
echo user_pref\(\"browser.download.folderList\",\ 2\)\; | tee -a "$firefox_prefsjs"

test -f "$list" || ( echo "файл со списком книг $list не найден" && exit 1 )
tmp_dir="/tmp/biblio-autoget"
mkdir -p ${tmp_dir} 2>/dev/null || true
# spellcheck порекомендовал :?, чтобы точно не выполнить rm -fvr /*
rm -fvr ${tmp_dir:?}/*

# перед запуском сбросим значение переменной DISPLAY на стандартное (полезно при ручной отладке скрипта)
export DISPLAY=:0

virt_display="$(( ( RANDOM % 100 )  + 1 ))"
echo "Random DISPLAY = $virt_display"

# Запустим отдельный икс-сервер, в котором без оконных менеджеров и прочих прибабмбасов будем запускать то, что нам нужно
# Использование отдельного X-сервера с заранее известным размером окна позволит нам спокойно использовать единый набор координат для xdotool (в какие точки внутри этого окна нажимать), которые не будут зависеть от размера экрана и иных параметров системы, где запускается этот скрипт
if [ "$1" = 'xvfb' ]
# если в качестве аргумента скрипту передано "xephyr", то запускаем графическое окно с отдельным икс-сервером; это для разработки скрипта
# иначе запускает фреймбуффер Xvfb, где не открывается никакое графическое окно; это для запуска на настоящем сервере
	then
		echo "Работаем в режиме Xvfb"
		Xvfb ":${virt_display}" -screen 0 1200x700x24 &
	else
		echo "Работаем в режиме Xephyr"
		Xephyr -br -ac -noreset -screen 1200x700 ":${virt_display}" &
fi
# PID запускаемых процессов записываем в файл, а не в переменную, чтобы можно было их все завершить даже после завершения этого скрипта, считав хранящиеся на диске значения
echo $! >${tmp_dir}/X-server.pid

# export DISPLAY делать ПОСЛЕ запуска Xephyr, иначе сам Xephyr пытается запуститься на еще не существующем DISPLAY
export DISPLAY=":${virt_display}"

hash1="$(head -n 1 "$list" | sort | uniq | rev | awk -F '/' '{print $1}' | rev)"
firefox -new-tab "https://biblio-online.ru/viewer/${hash1}/" &
echo "Был открыт Firefox, залогиньтесь на сайт и нажмите Enter для продолжения, как залогинитесь или если вы уже там залогинены... Для доступа к старницам книги нужно хотя бы раз в браузере открыть онлайн просомтрщик книги (вроде бы так, это не точно)."
read -p "Для продолжения работы нажмите Enter"

# теперь запустим Firefox; т.к. переменная окружения DISPLAY уже ранее была экспортирована, Firefox запустится именно в нашем виртуальном икс-сервере
for hash in $(cat "$list" | sort | uniq | rev | awk -F '/' '{print $1}' | rev)
do
	pkill firefox
	sleep 1
	pkill -9 firefox
	# wget -qO- does folow redirects; curl -s does not; we need to follow redirect to get the book's title
	# https://www.cyberciti.biz/tips/delete-leading-spaces-from-front-of-each-word.html , https://www.linuxquestions.org/questions/linux-newbie-8/sed-command-extract-contents-withing-body-tag-of-html-559019/#post2774377
	book_title="$(wget -qO- http://biblio-online.ru/viewer/${hash}/ | grep \<title\> | sed -e :a -e 's/<[^>]*>//g;/</N;//ba' | sed -e 's/^[ \t]*//')"
	# если не удалось извлечь название книги (переменная пустая), то названием будет хеш книги
	test -z "$book_title" && book_title="$hash"
	rm -fvr "${save_dir}/0" "${save_dir}/${book_title}"
	mkdir -p "${save_dir}/${book_title}"
	ln -s "${save_dir}/${book_title}" "${save_dir}/0" || exit 1
	firefox -new-tab "https://biblio-online.ru/viewer/${hash}/" &
	sleep 15
	for (( n=1; n <= 6; n++ ))
	do
		for (( k=1; k <= 50; k++ ))
		do
			xdotool click --clearmodifiers 5
			xdotool key Page_Down
		done
		sleep 3
	done
	#xdotool key Page_Up
	function firefox_download_page {
		firefox -new-tab "https://biblio-online.ru/viewer/getPage/${hash}/$1" &
		echo $! >${tmp_dir}/firefox_$i.pid
		if (( $i == 1 ))
			then sleep 15
			else sleep 1
		fi 
		# the return code this will be the return code of the whole fuction
		#test -f "${save_dir}/${book_title}/page_${i}.svg" 
		[ -f "${save_dir}/${book_title}/page_${i}.svg" ] || [ -f "${save_dir}/${book_title}/page_${i}.png" ] || [ -f "${save_dir}/${book_title}/page_${i}.jpg" ]
	}
	i=1
	while true
	do
		if firefox_download_page "$i"
			then
				# if firefox_fownload_page return code = 1 (test -f success)
				i=$[i + 1]
				continue #переход в начало цикла
			else
				sleep 5
				i_tmp=$[i + 1]
				if firefox_download_page "$i_tmp" 
					then continue
					else break #выход из цикла
				fi
		fi
	done
done


# подождем окончания всех подвисших загрузок в Firefox
sleep 20
# убъем все запущенные в ходе проверки процессы; убиваем в конце, а не в начале, чтобы в начале случайно не убить вообще другой процесс, у которого совпадет PID
for i in ${tmp_dir}/*.pid; do kill $(cat $i); done
# очистим за собой мусор
rm -fvr ${tmp_dir:?}/*

