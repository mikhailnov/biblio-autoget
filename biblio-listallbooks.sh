#!/bin/bash
# Dependencies: sudo apt install lynx
# составляет список идентификаторов всех имеющихся на сайте книг

rm -fv all_books_hashes.list
for (( i = 1; i < 22; i++ ))
do
	lynx -dump "https://biblio-online.ru/?page=$i" | awk '/http/{print $2}' | grep '/viewer/' | rev | awk -F '/' '{print $1}' | rev | grep -E "*-*-" | tee -a all_books_hashes.list
done

rm -fv books.list
while read line
do
	echo 'https://biblio-online.ru/book/'${line} | tee -a books.list
done < all_books_hashes.list
